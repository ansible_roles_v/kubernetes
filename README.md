Kubernetes
=========
Установка Kubernetes без создания и настройки кластера.  
Для создания и начальной настройки кластера воспользуйтесь ролью *K8s cluster*.  
Для добавления нод в кластер написана роль *K8s nodes*.  

Импорт роли
------------
```yaml
- name: kubernetes  
  src: https://gitlab.com/ansible_roles_v/kubernetes/  
  version: main  
```

Пример использования
--------------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - kubernetes
```